// Query Operators and Field Projection

// Mini Activity

db.products.insertMany([

	{
		"name" : "Iphone X",
		"price" : 30000,
		"isActive" : true
	},

	{
		"name" : "Samsung Galaxy S21",
		"price" : 51000,
		"isActive" : true
	},
	
	{
		"name" : "Razer Blackshark V2X",
		"price" : 2800,
		"isActive" : false
	},
	
	{
		"name" : "RAKK Gaming Mouse",
		"price" : 1800,
		"isActive" : true
	},
	
	{
		"name" : "Razer Mechanical Keyboard",
		"price" : 4000,
		"isActive" : true
	}
	
])

// Query Operators
	// allow for more flexible querying in MongoDB
	// instead of only being able to find/search for documents with exact values.
	// We can use query operators to define conditions instead of just specific criterias.

	// $gt, $lt, $gte, and $lte - (used with) numbers

		// $gt - greater than

		db.products.find({"price" : {$gt: 3000}})

		// $gte - greater than or equal to

		db.products.find({"price" : {$gte: 30000}})

		// $lt - less than

		db.products.find({"price" : {$lt: 4000}})

		// $lte - less than or equal to

		db.products.find({"price" : {$lte: 2800}})

	// Note: Query operators can be used to expand the search criteria for updating and deleting.

		db.products.updateMany(
			{"price" : {$gte: 30000}},
			{$set: {"isActive" : false}}
		)

	db.users.insertMany([

		{
			"firstName" : "Mary Jane",
			"lastName" : "Watson",
			"email" : "mjtiger@gmail.com",
			"password" : "tigerjackpot15",
			"isAdmin" : false
		},
		{
			"firstName" : "Gwen",
			"lastName" : "Stacy",
			"email" : "stacyTech@gmail.com",
			"password" : "stacyTech1991",
			"isAdmin" : true
		},
		{
			"firstName" : "Peter",
			"lastName" : "Parker",
			"email" : "peterWebDev@gmail.com",
			"password" : "webDeveloperPeter",
			"isAdmin" : true
		},
		{
			"firstName" : "Jonah",
			"lastName" : "Jameson",
			"email" : "jjjameson@gmail.com",
			"password" : "spideyisamenace",
			"isAdmin" : false
		},
		{
			"firstName" : "Otto",
			"lastName" : "Octavius",
			"email" : "ottoOctopi@gmail.com",
			"password" : "docOck15",
			"isAdmin" : true
		},
		
	])

	// $regex - This query operator will allow us to find documents in which it will match the characters/pattern of the character we are looking for.

		db.users.find(
			{"firstName" : {$regex: "O"}}
		)

		db.users.find(
			{"firstName" : {$regex: "W"}}
		)

	// $options
		
		// ($i cancels case-sensitivity)

		db.users.find({
			"lastName" : {$regex: 'o', $options: '$i'}
		})

	// find documents that match a specific word

		db.users.find({
			"email" : {$regex: 'web', $options: '$i'}
		})

		db.products.find({
			"name" : {$regex: 'phone', $options: '$i'}
		})

	// Mini Activity

		db.products.find({
			"name" : {$regex: 'razer', $options: "$i"}
		})
			db.products.find({
			"name" : {$regex: 'rakk', $options: "$i"}
		})


	// $ or and $and

		// $or operator - allows us to have a logical operation wherein we can look for or find a document which can satisfy at least one of our conditions.

		db.products.find({
			$or: [
				{
					"name" : {$regex: 'x', $options: '$i'}
				},
				{
					"price" : {$lte: 10000}
				}
			]
		})

		db.products.find({
			$or: [
				{
					"name" : {$regex: "x", $options: "$i"}
				},
				{
					"price" : 30000
				}
			]
		})

		db.users.find({
			$or: [
				{
					"firstName" : {$regex: "a", $options: "si"}
				},
				{
					"isAdmin" : true
				}
			]
		})

	// Mini Activity

		db.users.find({
			$and: [	
				{
					"lastName" : {$regex: "w", $options: "$i"}
				},
				{
					"isAdmin" : false
				}
			]
		})

	// $and operator - allows us to have a logical operation wherein we can look for or find documents which can satisfy both conditions.

		db.products.find({
			$and: [
				{
					"name" : {$regex: 'razer', $options: '$i'}
				},
				{
					"price" : {$gte: 3000}
				},
			]
		})

		db.users.find({
			$and: [
				{
					"lastName" : {$regex: 'w', $options: '$i'}
				},
				{
					"isAdmin" : false
				}
			]
		})

	// Mini Activity
	db.users.find({
		$and: [
			{
				"lastName" : {$regex: 'a', $options: '$i'}
			},
			{
				"firstName" : {$regex: 'e', $options: '$i'}
			}
		]
	})

// Field Projection
	// allows us to hide/show properties/field of the returned document after a query.

	// In field projection, 0 means hide and 1 means show.
	// db.collection.find({query}, {"field": 0/1})

	db.users.find({}, {"_id" : 0, "password" : 0})

	// find() can have two arguments.
		// db. collection.find({query}, {projection})

	db.users.find({"isAdmin" : false}, {"_id" : 0, "email" : 1})

	// (_id always shows itself by default.)
	// (If any field is set to 1, it will be the only field shown (by default, along with _id).)

	db.products.find(
		{
			"price" : {$gte: 10000}
		},
		{
			"_id" : 0, "name" : 1, "price" : 1
		}
	)